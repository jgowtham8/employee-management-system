package com.example.objectboxapp.interfaces

import com.example.objectboxapp.models.Employee

interface RecyclerViewItemClickListener {
    fun onUpdateBtnClicked(empModel: Employee)
    fun onDeleteBtnClicked(empModel: Employee)
}