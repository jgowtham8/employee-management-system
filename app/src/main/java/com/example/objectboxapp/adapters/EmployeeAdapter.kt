package com.example.objectboxapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.objectboxapp.R
import com.example.objectboxapp.interfaces.RecyclerViewItemClickListener
import com.example.objectboxapp.models.Employee
import kotlinx.android.synthetic.main.employee_row.view.*

class EmployeeAdapter(private val listOfObj: MutableList<Employee>, private val context: Context,
                      private val recyclerViewItemClickListener: RecyclerViewItemClickListener) : RecyclerView.Adapter<ViewHolder>() {

    // Inflates the item views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.employee_row,
                parent,
                false
            )
        )
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.name.text = listOfObj[position].name
        holder.email.text = listOfObj[position].email
        holder.address.text = listOfObj[position].address
        holder.salary.text = listOfObj[position].salary
        holder.department.text = listOfObj[position].department
        holder.rowUpdate.setOnClickListener {
            recyclerViewItemClickListener.onUpdateBtnClicked(listOfObj[position])
        }
        holder.rowDelete.setOnClickListener {
            recyclerViewItemClickListener.onDeleteBtnClicked(listOfObj[position])
        }

    }

    override fun getItemCount(): Int {
        return listOfObj.size
    }

}

class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
    val name: TextView = view.tvName
    val email: TextView = view.tvEmail
    val address: TextView = view.tvAddress
    val salary: TextView = view.tvSalary
    val department: TextView = view.tvDepartment
    val rowUpdate: Button = view.update
    val rowDelete: Button = view.delete

}








//    (resultSet: MutableList<Employee>) : RecyclerView.Adapter<EmployeeAdapter.EmpViewHolder>() {
//
//    private val empList = resultSet
//
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmpViewHolder {
//        return EmpViewHolder(
//            LayoutInflater.from(parent.context).inflate(
//                R.layout.employee_row,
//                parent,
//                false
//            )
//        )
//    }
//
//    override fun getItemCount(): Int {
//        return empList.size
//    }
//
//    override fun onBindViewHolder(holder: EmpViewHolder, position: Int) {
//        holder.bind(empList[position])
//    }
//
//    inner class EmpViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
//
//        fun bind(empModel: Employee) {
//            itemView.tvName.text = empModel.name
//            itemView.tvEmail.text = empModel.email
//            itemView.tvAddress.text = empModel.address
//            itemView.tvSalary.text = empModel.salary
//            itemView.tvDepartment.text = empModel.department
//
//        }
//    }
//}

