package com.example.objectboxapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.objectboxapp.R
import com.example.objectboxapp.models.User
import kotlinx.android.synthetic.main.user_list_item.view.*

class UserAdapter(private val listOfObj: MutableList<User>, private val context: Context) : RecyclerView.Adapter<ViewHolder2>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder2 {
        return ViewHolder2(
            LayoutInflater.from(context).inflate(
                R.layout.user_list_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return listOfObj.size
    }

    override fun onBindViewHolder(holder: ViewHolder2, position: Int) {
        holder.name.text = listOfObj[position].name
        holder.email.text = listOfObj[position].email
        holder.fullName.text = listOfObj[position].fullName
        holder.password.text = listOfObj[position].password
    }
}

class ViewHolder2 (view: View) : RecyclerView.ViewHolder(view) {
    val name: TextView = view.tvUName
    val fullName: TextView = view.tvUFullName
    val password: TextView = view.tvUPassword
    val email: TextView = view.tvUEmail

}