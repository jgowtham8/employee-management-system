package com.example.objectboxapp.login_logout

import com.example.objectboxapp.activities.ActivityMenu

class Logout {
    public fun logout(){
        var globalState = Login()
        globalState.loggedUserName = ""
    }
}