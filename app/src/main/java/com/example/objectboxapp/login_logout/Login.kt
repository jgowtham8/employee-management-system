package com.example.objectboxapp.login_logout

import android.util.Log
import com.example.objectboxapp.activities.ActivityMenu
import com.example.objectboxapp.models.User
import com.example.objectboxapp.models.User_
import com.example.objectboxapp.object_box.ObjectBox
import io.objectbox.Box
import io.objectbox.kotlin.boxFor
import io.objectbox.kotlin.query
import io.objectbox.query.Query




class Login {
    private lateinit var userBox: Box<User>
    private lateinit var query: Query<User>
    public var loggedUserName = ""

    public fun login(txtUserName:String, txtPassword:String): Boolean {
        var state = false
        userBox = ObjectBox.boxStore.boxFor()

        query = userBox.query {
            equal(User_.name,txtUserName)
            equal(User_.password,txtPassword)
        }

        val user = query.find()
        Log.d("ObjectBoxExample", "User count: " + user.size)

        if (user.size > 0){
            state = true
            loggedUserName = user[0].name.toString()
        }
        return state

    }
}