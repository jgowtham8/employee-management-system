package com.example.objectboxapp.models

import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id

@Entity
data class Employee (
    @Id var id: Long = 0,
    var name: String? = null,
    var email: String? = null,
    var address: String? = null,
    var salary: String? = null,
    var department: String? = null
)