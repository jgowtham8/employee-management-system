package com.example.objectboxapp.models

import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id

@Entity
data class User (
    @Id var id: Long = 0,
    var name: String? = null,
    var fullName: String? = null,
    var password: String? = null,
    var email: String? = null
)