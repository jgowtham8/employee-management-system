package com.example.objectboxapp.activities.user

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.objectboxapp.R
import com.example.objectboxapp.adapters.EmployeeAdapter
import com.example.objectboxapp.adapters.UserAdapter
import com.example.objectboxapp.models.Employee
import com.example.objectboxapp.models.Employee_
import com.example.objectboxapp.models.User
import com.example.objectboxapp.models.User_
import com.example.objectboxapp.object_box.ObjectBox
import io.objectbox.Box
import io.objectbox.kotlin.boxFor
import io.objectbox.kotlin.query
import io.objectbox.query.Query
import kotlinx.android.synthetic.main.activity_list.*
import kotlinx.android.synthetic.main.activity_user_list.*

class ActivityUserList : AppCompatActivity() {

    private lateinit var userBox: Box<User>
    private lateinit var userQuery: Query<User>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_list)

        userBox = ObjectBox.boxStore.boxFor()

        userQuery = userBox.query {
            order(User_.name)
        }

        val user = userQuery.find()

        //loading emp list to recycler view
        recyclerViewUserList.layoutManager = LinearLayoutManager(this)
        recyclerViewUserList.adapter = UserAdapter(user, this)
    }
}
