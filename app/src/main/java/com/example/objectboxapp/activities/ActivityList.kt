package com.example.objectboxapp.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.objectboxapp.models.Employee
import com.example.objectboxapp.adapters.EmployeeAdapter
import com.example.objectboxapp.object_box.ObjectBox
import com.example.objectboxapp.R
import com.example.objectboxapp.activities.user.ActivityLogin
import com.example.objectboxapp.interfaces.RecyclerViewItemClickListener
import com.example.objectboxapp.login_logout.Logout
import com.example.objectboxapp.models.Employee_
import io.objectbox.Box
import io.objectbox.kotlin.boxFor
import io.objectbox.kotlin.query
import io.objectbox.query.Query
import kotlinx.android.synthetic.main.activity_list.*

class ActivityList : AppCompatActivity() {

    private lateinit var employeeAdapter: EmployeeAdapter
    private lateinit var employeeBox: Box<Employee>
    private lateinit var employeeQuery: Query<Employee>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)

        employeeBox = ObjectBox.boxStore.boxFor()

        employeeQuery = employeeBox.query {
            order(Employee_.name)
        }

        val emp = employeeQuery.find()

        //loading emp list to recycler view
        recyclerViewList.layoutManager = LinearLayoutManager(this)
        recyclerViewList.adapter = EmployeeAdapter(emp, this, object : RecyclerViewItemClickListener {
            override fun onUpdateBtnClicked(empModel: Employee) {
                goToActivityUpdate(empModel.id)
            }
            override fun onDeleteBtnClicked(empModel: Employee) {
                goToAlertAndDelete(empModel.id)
            }
        })

        listLogout.setOnClickListener{
            val stateLogout = Logout()
            stateLogout.logout()

            val go = Intent(this, ActivityLogin::class.java)
            startActivity(go)
            this.finish()
        }
        listAddNew.setOnClickListener {
            val go = Intent(this, ActivityAdd::class.java)
            startActivity(go)
            this.finish()
        }
    }


    private fun goToActivityUpdate(id: Long) {
        val intent = Intent(this, ActivityUpdate::class.java)
        if (id != null) {
            intent.putExtra("empID", id)
        }
        startActivity(intent)
        this.finish()
    }

    private fun goToAlertAndDelete(id: Long) {
        val empToDelete = employeeBox.get(id)

        if (empToDelete != null) {
            val mAlertDialog = AlertDialog.Builder(this@ActivityList)
            mAlertDialog.setTitle("Warning..!") //set alert dialog title
            mAlertDialog.setMessage("Are you sure to delete "+empToDelete.name+"?") //set alert dialog message
            mAlertDialog.setPositiveButton("Yes") { dialog, id ->
                employeeBox.remove(empToDelete!!)
                Toast.makeText(this,  "Emp Removed " +
                        empToDelete?.name, Toast.LENGTH_LONG).show()

                val go = Intent(this, ActivityList::class.java)
                startActivity(go)

                this.finish()
            }
            mAlertDialog.setNegativeButton("No") { dialog, id ->   }
            mAlertDialog.setNeutralButton("Cancel"){dialog, id ->  }
            mAlertDialog.show()
        }
    }
}
