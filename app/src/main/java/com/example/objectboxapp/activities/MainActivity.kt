package com.example.objectboxapp.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.objectboxapp.R
import com.example.objectboxapp.activities.user.ActivityLogin
import com.example.objectboxapp.adapters.EmployeeAdapter
import com.example.objectboxapp.object_box.ObjectBox
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var empAdapter: EmployeeAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ObjectBox.init(this)

        getStarted.setOnClickListener {
            val go = Intent(this, ActivityLogin::class.java)
            startActivity(go)
        }
    }
}
