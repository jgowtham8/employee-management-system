package com.example.objectboxapp.activities.user

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.example.objectboxapp.R
import com.example.objectboxapp.models.User
import com.example.objectboxapp.models.User_
import com.example.objectboxapp.object_box.ObjectBox
import io.objectbox.Box
import io.objectbox.kotlin.boxFor
import io.objectbox.kotlin.query
import io.objectbox.query.Query
import kotlinx.android.synthetic.main.activity_sign_up.*

class ActivitySignUp : AppCompatActivity() {
    private lateinit var userBox: Box<User>
    private lateinit var query: Query<User>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        userBox = ObjectBox.boxStore.boxFor()

        btnSignUp.setOnClickListener {
            val name = etUName.text.toString()
            val email = etUEmail.text.toString()
            val fullName = etFullName.text.toString()
            val password = etPassword.text.toString()

            userBox = ObjectBox.boxStore.boxFor()
            query = userBox.query {
                equal(User_.name,name)
            }
            val userCheck = query.find()

            if (userCheck.size > 0){
                Toast.makeText(applicationContext, "Username already exists..!", Toast.LENGTH_SHORT).show()
            }
            else if (name == ""){
                Toast.makeText(applicationContext, "Username field is empty..!", Toast.LENGTH_SHORT).show()
            }
            else{
                resetFields()

                val user =
                    User(
                        name = name,
                        email = email,
                        fullName = fullName,
                        password = password
                    )
                userBox.put(user)

                Toast.makeText(applicationContext, "New User Added..\n"+user.id, Toast.LENGTH_SHORT).show()

                Log.d("ObjectBoxExample", "Inserted, ID: " + user.id)

                val go = Intent(this, ActivityLogin::class.java)
                startActivity(go)
                this.finish()
            }



        }
    }
    public fun resetFields(){
        etFullName.setText("")
        etUName.setText("")
        etPassword.setText("")
        etUEmail.setText("")
    }
}
