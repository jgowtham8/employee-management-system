package com.example.objectboxapp.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.objectboxapp.R
import com.example.objectboxapp.activities.user.ActivityLogin
import com.example.objectboxapp.activities.user.ActivityUserList
import com.example.objectboxapp.login_logout.Login
import com.example.objectboxapp.login_logout.Logout
import kotlinx.android.synthetic.main.activity_menu.*

class ActivityMenu : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        val logged = intent.extras?.getString("userID")
        loggedIn.text = logged

        add.setOnClickListener {
            val go = Intent(this, ActivityAdd::class.java)
            startActivity(go)
        }

        list.setOnClickListener {
            val go = Intent(this, ActivityList::class.java)
            startActivity(go)
        }

        signOut.setOnClickListener {
            val stateLogout = Logout()
            stateLogout.logout()

            val go = Intent(this, ActivityLogin::class.java)
            startActivity(go)
            this.finish()
        }

        UserList.setOnClickListener {
            val go = Intent(this, ActivityUserList::class.java)
            startActivity(go)
        }
    }
}
