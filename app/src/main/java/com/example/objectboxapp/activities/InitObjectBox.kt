package com.example.objectboxapp.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.objectboxapp.object_box.ObjectBox

class InitObjectBox : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ObjectBox.init(this)
    }
}
