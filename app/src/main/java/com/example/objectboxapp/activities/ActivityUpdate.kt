package com.example.objectboxapp.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.example.objectboxapp.R
import com.example.objectboxapp.models.Employee
import com.example.objectboxapp.object_box.ObjectBox
import kotlinx.android.synthetic.main.activity_update.*
import kotlinx.android.synthetic.main.employee_row.*

class ActivityUpdate : AppCompatActivity() {

    private val empBox = ObjectBox.boxStore.boxFor(Employee::class.java)
    private var empToUpdate: Employee? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update)

        val idForUpdate: Long? = intent.extras?.getLong("empID")

        if (idForUpdate != null) {
            empToUpdate = empBox.get(idForUpdate)

            etName.setText(empToUpdate?.name)
            etEmail.setText(empToUpdate?.email)
            etAddress.setText(empToUpdate?.address)
            etSalary.setText(empToUpdate?.salary)
            etDepartment.setText(empToUpdate?.department)
        }

        btnUpdate.setOnClickListener {
            val name = etName.text.toString()
            val email = etEmail.text.toString()
            val address = etAddress.text.toString()
            val salary = etSalary.text.toString()
            val department = etDepartment.text.toString()

            val emp =
                Employee(
                    id = idForUpdate!!.toLong(),
                    name = name,
                    email = email,
                    address = address,
                    salary = salary,
                    department = department
                )
            empBox.put(emp)

            Toast.makeText(applicationContext, "Employee Updated..", Toast.LENGTH_SHORT).show()

            Log.d("ObjectBoxExample", "Updated, ID: " + emp.id)

            val go = Intent(this, ActivityList::class.java)
            startActivity(go)
            this.finish()

        }
    }
}
