package com.example.objectboxapp.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.example.objectboxapp.models.Employee
import com.example.objectboxapp.object_box.ObjectBox
import com.example.objectboxapp.R
import io.objectbox.Box
import io.objectbox.kotlin.boxFor
import kotlinx.android.synthetic.main.activity_add.*

class ActivityAdd : AppCompatActivity() {

    private lateinit var employeeBox: Box<Employee>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add)

        employeeBox = ObjectBox.boxStore.boxFor()

        btnAdd.setOnClickListener {
            val name = etName.text.toString()
            val email = etEmail.text.toString()
            val address = etAddress.text.toString()
            val salary = etSalary.text.toString()
            val department = etDepartment.text.toString()

            resetFields()

            val emp =
                Employee(
                    name = name,
                    email = email,
                    address = address,
                    salary = salary,
                    department = department
                )
            employeeBox.put(emp)

            Toast.makeText(applicationContext, "Employee Added..", Toast.LENGTH_SHORT).show()

            Log.d("ObjectBoxExample", "Inserted, ID: " + emp.id)

            val go = Intent(this, ActivityList::class.java)
            startActivity(go)
            this.finish()

        }
    }

    public fun resetFields(){
        etName.setText("")
        etEmail.setText("")
        etAddress.setText("")
        etSalary.setText("")
        etDepartment.setText("")
    }
}
