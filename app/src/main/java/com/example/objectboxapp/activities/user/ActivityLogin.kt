package com.example.objectboxapp.activities.user

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.objectboxapp.R
import com.example.objectboxapp.models.Employee_
import com.example.objectboxapp.models.User
import com.example.objectboxapp.models.User_
import com.example.objectboxapp.object_box.ObjectBox
import io.objectbox.Box
import io.objectbox.Property
import io.objectbox.kotlin.boxFor
import io.objectbox.kotlin.query
import io.objectbox.query.Query
import io.objectbox.query.QueryBuilder
import kotlinx.android.synthetic.main.activity_login.*
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.widget.Toast
import com.example.objectboxapp.activities.ActivityMenu
import com.example.objectboxapp.login_logout.Login
import com.example.objectboxapp.models.MyObjectBox
import com.example.objectboxapp.object_box.ObjectBox.boxStore


class ActivityLogin : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)



        btnLogin.setOnClickListener{
            val username = etULName.text.toString()
            val password = etULPassword.text.toString()

            val state = Login()
            if (state.login(username,password)){
                val go = Intent(this, ActivityMenu::class.java)
                go.putExtra("userID", username)
                startActivity(go)
            }
            else{
                Toast.makeText(this,  "Invalid Login..!", Toast.LENGTH_LONG).show()
            }
        }

        btnSignUp.setOnClickListener {
            val go = Intent(this, ActivitySignUp::class.java)
            startActivity(go)
        }


    }
}
